```plantuml
box "app_db_write_batch"
activate APP
APP  ->  GMDB_API:  db_batch_execute(object)
==  check_global_flag  ==
GMDB_API -> STLM_API: <color:blue> chk_stlm_flag
activate STLM_API

STLM_API -->GMDB_API: global_flag
deactivate STLM_API

==  log_handler  ==
alt global_log_flag_on

    GMDB_API -> STLM_API: <color:blue> batch_log_handler(batch_obj)
    activate STLM_API
    STLM_API -> STLM_API : check_log_filter_batch

    alt  log_filter_match
        STLM_API -> STLM: log_object_before
        'STLM -->STLM_API: log_object__before_ret
    end

    STLM_API --> GMDB_API: log_handler_ret:bitmap
    deactivate STLM_API

    GMDB_API->GMDB_API: store_log_bitmap

end
==  trace_handler  ==
alt  global_trace_flag_on
    GMDB_API -> STLM_API: <color:blue> batch_trace_handler(batch_object)
    activate STLM_API
    STLM_API->STLM_API: check_trace_filter_batch



    STLM_API -> STLM_API: build_span_context
    STLM_API -->GMDB_API: context_list_with_bitmap
    deactivate STLM_API


    alt  span_context_list_not_null
        GMDB_API ->GMDB_API: store_trace_bitmap

        GMDB_API ->GMDB_API: for_each_obj:tracer_inject(object,context)
    end

end
==  db_operate  ==
GMDB_API -> GMDB: db_operate(object)
GMDB -->GMDB_API: db_operate_ret
==  log_finish  ==
GMDB_API -> STLM_API: <color:blue> batch_log_after_callback
activate STLM_API
STLM_API -> GMDB_API: read_record
GMDB_API --> STLM_API: record_obj
STLM_API -> STLM: log_object_after
deactivate STLM_API

'STLM -->STLM_API: log_object__before_ret

==  trace_finish  ==
GMDB_API -> STLM_API: <color:blue> batch_trace_finish_callback 
STLM_API -> STLM: report_span
STLM_API -> STLM: write_trace_ref

GMDB_API -->APP: db_operate_ret
deactivate APP
end box
```